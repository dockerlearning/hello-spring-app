/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.namnt.hellospring;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Nguyen The Nam
 */
public interface CustomersRepository extends CrudRepository<Customers, Integer> {

    @Query(value = "SELECT * FROM customers c WHERE name LIKE ?1",
            nativeQuery = true)
    public List<Customers> getListCustomer(String query, Pageable pageable);
    
    @Query(value = "SELECT count(*) FROM customers WHERE name LIKE ?1",
            nativeQuery = true)
    public int countListCustomer(String query);
}
