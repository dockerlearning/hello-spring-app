FROM openjdk:8u191-jre-alpine3.9
MAINTAINER namnt.net

VOLUME /tmp

RUN apk --no-cache add maven && mvn --version
RUN mvn clean install -DskipTests

COPY target/hellospringapp-0.0.1-SNAPSHOT.jar hellospring.jar
ENTRYPOINT exec java -jar hellospring.jar