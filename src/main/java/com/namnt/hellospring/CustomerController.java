/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.namnt.hellospring;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nguyen The Nam
 */
@RestController
@RequestMapping(path = "/customer")
public class CustomerController {

    @Autowired
    private CustomersRepository customersRepository;

//    @RequestMapping("/list1")
//    public ResponseEntity<List<Customers>> listCustomers1() {
//        List<Customers> listCustomers = (List<Customers>) customersRepository.findAll();
//        return new ResponseEntity<>(listCustomers, HttpStatus.OK);
//    }

    @RequestMapping("/list")
    public ResponseEntity<CustomersWrapper> listCustomers2(
            @RequestParam(name = "query", required = false, defaultValue = "") String query,
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "5") Integer size,
            @RequestParam(name = "sort", required = false, defaultValue = "ASC") String sort) {
        Sort sortable = null;
        if (sort.equals("ASC")) {
            sortable = Sort.by("name").ascending();
        }
        if (sort.equals("DESC")) {
            sortable = Sort.by("name").descending();
        }
        Pageable pageable = PageRequest.of(page, size, sortable);
        List<Customers> listCustomers = customersRepository.getListCustomer("%" + query + "%", pageable);
        int totalCustomers = customersRepository.countListCustomer("%" + query + "%");
        CustomersWrapper cw = new CustomersWrapper();
        cw.setTotal(totalCustomers);
        cw.setData(listCustomers);
        return new ResponseEntity<>(cw, HttpStatus.OK);
    }
}
